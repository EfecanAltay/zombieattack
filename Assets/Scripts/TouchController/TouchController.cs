﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TouchController : MonoBehaviour {
    public Text log;
    public GameObject movingController;
    bool openingMovingController = false;
    Vector2 movingStartPos;
    public PlayerController playerController;
    public RectTransform movingControlStick;
    public RectTransform rotatingControlStick;

    void Start () {
		
	}
	void Update () {
        MovingControllerUpdate();
        //RotatingControllerUpdate();
    }
    public void MovingControllerUpdate() {
        Vector2 p = movingControlStick.anchoredPosition;

        if (p.x > 15f)
            playerController.Move(Vector2.right);
        else if (p.x < -15f)
            playerController.Move(Vector2.left);

        if (p.y > 15f)
            playerController.Move(Vector2.up);
        else if (p.y < -15f)
            playerController.Move(Vector2.down);
    }
    public void RotatingControllerUpdate()
    {
        Vector2 p = rotatingControlStick.anchoredPosition;

        float angle = Mathf.Atan(p.y / p.x) * Mathf.Rad2Deg;
        if (p.x < 0)
        {
            if (p.y < 0)
            {
                angle -= 180;
            }
            else
            {
                angle += 180;
            }
        }
        if (p.x > 15f)
            playerController.Shoot();
        else if (p.x < -15f)
            playerController.Shoot();

        if (p.y > 15f)
            playerController.Shoot();
        else if (p.y < -15f)
            playerController.Shoot();

        //Debug.Log("Angle : " + angle + " sw : " + Screen.width + " mw : " + Input.mousePosition.x);
        playerController.Rotate(Quaternion.AngleAxis(angle, Vector3.forward));

    }
    void onTouch () {
        if (Input.touches.Length > 0)
        {
            Touch t = Input.GetTouch(0);
            Debug.Log(t.position);
            if (openingMovingController)
            {

            }
            else
            {
                if (t.position.x < Screen.width / 2.5f)
                {
                    movingController.SetActive(true);
                    openingMovingController = true;
                    movingStartPos = t.position;
                    movingController.GetComponent<RectTransform>().anchoredPosition = t.position;

                }
            }
            log.text = "touch pos : " + t.position;
        }
        else
        {
            //movingController.SetActive(false);
            openingMovingController = false;
        }
    }

}
