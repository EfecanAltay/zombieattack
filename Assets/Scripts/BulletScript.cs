﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {

	
	void Start () {
        Destroy(gameObject, 2f);	
	}

    void Update () {
		
	}
    private void OnCollisionEnter2D(Collision2D collision)
    {
        string tag = collision.gameObject.tag ;
        if (gameObject != null)
        {
            if (tag != "Player")
            {
                if (tag != "Enemy")
                    gameObject.GetComponent<ParticleSystem>().Play();

                gameObject.GetComponent<SpriteRenderer>().enabled = false;
                //gameObject.GetComponent<SpriteRenderer>().enabled = false;
                gameObject.GetComponent<CircleCollider2D>().enabled = false;
                gameObject.GetComponent<Rigidbody2D>().Sleep();
            }
            switch (tag)
            {
                case "Enemy":
                    collision.gameObject.GetComponent<EnemyFunctions>().enemy.HealDown(45f);
                    break;
                default:
                    break;
            }
        }
        
    }
}
