﻿using UnityEngine;

public class GameControl : MonoBehaviour {

    public delegate void GamePlayChangeEvent(bool GameisPlay);
    public delegate void GameModeEvent();

    public event GameModeEvent GameStartingCallback;
    public event GamePlayChangeEvent GamePlayChangeCallback;
    public event GameModeEvent GameOverCallback;

    bool gameIsPlay;

    public void FirstStartGame()
    {
        if( GameStartingCallback != null) GameStartingCallback();
    }
    public void PauseGame()
    {
        gameIsPlay = false;
        if (GamePlayChangeCallback != null) GamePlayChangeCallback(gameIsPlay);
    }
    public void ResumeGame()
    {
        gameIsPlay = true;
        if (GamePlayChangeCallback != null) GamePlayChangeCallback(gameIsPlay);
    }
    public void GameOver()
    {
        if (GameOverCallback != null) GameOverCallback();
    }
}
