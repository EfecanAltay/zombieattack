﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    Rigidbody2D rigidbody2D;
    float walkSpeed = 30f;
    Vector2 rot;
    public PlayerFunctions pFunctions;
    void Start () {
        rigidbody2D = this.gameObject.GetComponent<Rigidbody2D>();
        Input.multiTouchEnabled = true;
	}
	
	// Update is called once per frame
	void Update () {
        ShootInputs();
        Movement();

    }
    public void Move(Vector2 vector)
    {
        rot = vector ;
        if (rot != Vector2.zero)
            rigidbody2D.AddForce(rot * walkSpeed, ForceMode2D.Force);
    }
    public void Rotate(Quaternion rotation)
    {
        gameObject.transform.rotation = rotation;
    }
    
    void Movement()
    {
        
        if (Input.anyKey)
        {
            //rigidbody2D.Sleep();
            rot = Vector2.zero;
            if (Input.GetKey(KeyCode.W))
            {
                rot = Vector2.up;
            }
            else if (Input.GetKey(KeyCode.S))
            {
                rot = Vector2.down;
            }
            if (Input.GetKey(KeyCode.D))
            {
                rot += Vector2.right;
            }
            else if (Input.GetKey(KeyCode.A))
            {
                rot += Vector2.left;
            }
            if(rot != Vector2.zero)
            rigidbody2D.AddForce(rot * walkSpeed , ForceMode2D.Force);
        }
        else {
            //rigidbody2D.Sleep();
            //rigidbody2D.AddForce(-rot, ForceMode2D.Impulse);
           // rigidbody2D.
        }
    }
    public void Shoot() {
        pFunctions.Fire();
    }
    void ShootInputs() {
        if (Input.GetButton("Fire1"))
        {
            pFunctions.Fire();
        }
        else if (Input.GetButton("Fire2")) {
            Debug.Log("Fire 2");
        }
    }
}