﻿using UnityEngine;
using UnityEngine.UI;


public class PlayerHud : MonoBehaviour {

    Player playerInfos;

    public Image HealthBar,EnergyBar;
    public GameObject GameOverMenu;
    public GameControl gameControl;
    public Image Effect;
	void Start () {
        playerInfos = GameObject.FindWithTag("Player").GetComponent<PlayerFunctions>().player;
        playerInfos.DeadCallback += PlayerInfos_DeadCallback;
        playerInfos.TakeEnergyCallBack += PlayerInfos_TakeEnergyCallBack;
	}

    private void PlayerInfos_TakeEnergyCallBack()
    {

    }

    private void PlayerInfos_DeadCallback()
    {
        GameOverMenu.SetActive(true);
        gameControl.GameOver();
    }

    void Update () {
        HealthBar.rectTransform.sizeDelta = new Vector2(playerInfos.HealthPoint * 451 / 100,37);
        EnergyBar.rectTransform.sizeDelta = new Vector2(playerInfos.EnergyTimer * 371 / 5, 28);
    }
}
