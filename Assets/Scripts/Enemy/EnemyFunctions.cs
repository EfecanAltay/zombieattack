﻿using UnityEngine;

public class EnemyFunctions : MonoBehaviour
{
    public Enemy enemy = new Enemy();
    public GameControl gControl;

    float AttackTimer = 0f;
    bool attackable = false;

    EnemyMoving enemyMoving;
    public Sprite enemyDeadSprites;

    public GameObject HPotionObject , EPotionObject;
    public int Health = 100;
    //--------****---------------
    bool functionsActive = false;

    private void Start()
    {
        enemyMoving = GetComponent<EnemyMoving>();
        gControl = GameObject.Find("GameControl").GetComponent<GameControl>();
        gControl.GameOverCallback += GControl_GameOverCallback;
        enemy.DeadCallback += Enemy_DeadCallback;
        enemy.MaxHealthPoint = Health ;
        enemy.HealthPoint = Health;

        functionsActive = true;
    }

    private void Enemy_DeadCallback()
    {
        Dead();
    }

    private void Update()
    {
        if (AttackTimer > 0)
        {
            attackable = false;
            AttackTimer -= Time.deltaTime;
        }else
        {
            AttackTimer = 0 ;
            attackable = true;
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (functionsActive)
        {
            if (collision.gameObject.tag == "Player")
            {
                if (attackable)
                {
                    GameObject pgo = collision.gameObject;
                    float h = enemy.Attack(pgo.GetComponent<PlayerFunctions>().player);
                    Debug.Log("Attack to player : " + h);
                    AttackTimer = enemy.AttackTime;
                }
            }
        }
    }
    private void GControl_GameOverCallback()
    {
        Debug.Log("GameOver");
        functionsActive = false;
        enemyMoving.isMovable = false;
    }
    void Dead()
    {
        GameObject go = new GameObject("Enemy", typeof(SpriteRenderer));
        go.transform.position = gameObject.transform.position;
        go.transform.rotation = gameObject.GetComponentInChildren<SpriteRenderer>().transform.rotation;

        SpriteRenderer sp = go.GetComponent<SpriteRenderer>();
        sp.sprite = enemyDeadSprites;
        sp.sortingOrder = 3;
        sp.color = new Color(.7f,.7f,.7f,1f);
        if (Random.Range(0f, 1f) > .55f)
        {
            if (Random.Range(0f, 1f) > .2f)
                Instantiate(HPotionObject, transform.position, transform.rotation, null);
            else
                Instantiate(EPotionObject, transform.position, transform.rotation, null);
        }
        //Instantiate(go, gameObject.transform.position,gameObject.transform.rotation);
        Destroy(gameObject);
    }
}
