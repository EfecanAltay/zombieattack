﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHUD : MonoBehaviour {

    Enemy enemy;
    public ParticleSystem HealthDownPSystem;
    public GameObject healthbar;
	void Start () {
        enemy = gameObject.GetComponent<EnemyFunctions>().enemy;
        enemy.HealthDownCallback += Enemy_HealthDownCallback;
    }

    private void Enemy_HealthDownCallback(float h)
    {
        UpdateHUD();
        HealthDownPSystem.Play();
    }

    void Update () {
		
	}

    void UpdateHUD()
    {
        healthbar.transform.localScale = new Vector3(5*enemy.HealthPoint/enemy.MaxHealthPoint, 0.8f,1f);
    }
}
