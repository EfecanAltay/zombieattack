﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMoving : MonoBehaviour {

    public GameObject playerObject;
    Transform targetTransform;
    Vector2 movingDirection;
    public float enemySpeed = .1f;
    float angle;
    Rigidbody2D rigidbody2d;

    public bool isMovable = true;
    void Start () {
        playerObject = GameObject.FindWithTag("Player");
        targetTransform = playerObject.transform;
        //rigidbody2d = GetComponent<Rigidbody2D>();
    }

    void Update () {
        if (isMovable)
        {
            movingDirection = playerObject.transform.position - gameObject.transform.position;
            Debug.DrawRay(transform.position, movingDirection, Color.red);
            angle = Mathf.Atan(movingDirection.y / movingDirection.x) * Mathf.Rad2Deg;
            if (transform.position.x > playerObject.transform.position.x)
            {
                if (transform.position.y > playerObject.transform.position.y)
                {
                    angle -= 180;
                }
                else
                {
                    angle += 180;
                }
            }
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

            transform.Translate(Vector3.right * Time.deltaTime * enemySpeed);
        }
    }
    
}
